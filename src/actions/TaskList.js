import { TaskListActions } from '../constants';

export function fetchCurrentTaskList(taskList) {
    return {
        type: TaskListActions.TASK_LIST_SET,
        payload: taskList,
    };
}

export function fetchAddTask(task) {
    return {
        type: TaskListActions.TASK_LIST_ADD,
        payload: task,
    };
}

export function fetchEditTask(task, id) {
    return {
        type: TaskListActions.TASK_LIST_EDIT,
        payload: { task, id },
    };
}

export function fetchTitle(title) {
    return {
        type: TaskListActions.TASK_LIST_SET_TITLE,
        payload: title,
    };
}

export function fetchCaptionButton(caption) {
    return {
        type: TaskListActions.TASK_LIST_SET_CAPTION_BUTTON,
        payload: caption,
    };
}
