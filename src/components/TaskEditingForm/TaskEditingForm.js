import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import moment from 'moment';

import {
    fetchCurrentTaskList,
    fetchAddTask,
    fetchEditTask,
} from '../../actions/TaskList';
import {
    getTaskList,
    getTitle,
    getCaptionAddTaskButton,
} from '../../reducers/TaskList';

import { importanceOfTasks } from '../../dictionary';

import './TaskEditingForm.css';

class TaskEditingForm extends Component {
    state = {
        taskName: '',
        descriptionTask: '',
        importanceOfTasksId: 1,
        finishDateTimeForTask: moment().format('YYYY-MM-DDTHH:mm'),
        dateTimeDoneTask: null,
        done: false,
    };

    componentDidMount() {
        const { match: { params }, taskList } = this.props;

        if (taskList) {
            const currentTask = taskList[parseInt(params.id)];
            this.setState(currentTask);
        }
    }

    //метод для отметы
    handlerClose = () => {
        const { history } = this.props;
        history.push('/');
    };

    // метод для добавления/редактирования задач
    handlerAddEditTask = event => {
        const { actions, match: { params } } = this.props;
        const task = { ...this.state };
        if (params.id) {
            actions.fetchEditTask(task, params.id);
        } else {
            actions.fetchAddTask(task);
        }
        this.handlerClose();
    };

    // метод для изменения значения названия задачи
    handlerChangeTaskName = event => {
        const { value } = event.target;
        this.setState({ taskName: value });
    };

    // метод для изменения значения описания задачи
    handlerChangeDescriptionTask = event => {
        const { value } = event.target;
        this.setState({
            descriptionTask: value,
        });
    };

    // метод для изменения значения важности задачи
    handlerChangeImportanceOfTask = event => {
        const { value } = event.target;
        this.setState({
            importanceOfTasksId: value,
        });
    };

    /**
     *  метод для изменения значения даты и времени
     * до которой нужно выполнить задачу задачи
     */
    handlerChangeFinishDateTimeForTask = event => {
        const { value } = event.target;
        this.setState({
            finishDateTimeForTask: value,
        });
    };

    // метод для изменения состояния задачи
    handlerChangeDone = event => {
        const { done } = this.state;
        let dateTimeDoneTask;
        const newDone = !done;
        if (newDone) {
            dateTimeDoneTask = moment().format('YYYY-MM-DDTHH:mm');
        } else {
            dateTimeDoneTask = '';
        }
        this.setState({
            done: newDone,
            dateTimeDoneTask,
        });
    };

    render() {
        const { title, captionAddTaskButton } = this.props;

        return (
            <div className="form-root">
                <form
                    className="form-container"
                    onSubmit={this.handlerAddEditTask}
                >
                    <div className="form-title">{title}</div>
                    <div className="form-container-input">
                        <input
                            key="taskName"
                            id="taskName"
                            className="form-input"
                            type="text"
                            placeholder="Введите название задачи"
                            onChange={this.handlerChangeTaskName}
                            value={this.state.taskName || ''}
                        />
                    </div>
                    <div className="form-container-textarea">
                        <textarea
                            key="descriptionTask"
                            id="descriptionTask"
                            className="form-textarea"
                            placeholder="Введите описание задачи"
                            value={this.state.descriptionTask || ''}
                            onChange={this.handlerChangeDescriptionTask}
                        />
                    </div>
                    <div className="form-container-input">
                        <label for="importanceOfTask" className="form-label">
                            Важность задачи:
                        </label>
                        <select
                            key="importanceOfTask"
                            id="importanceOfTask"
                            className="form-select"
                            placeholder="Выберите важность задачи"
                            onChange={this.handlerChangeImportanceOfTask}
                        >
                            {importanceOfTasks.map(
                                importtance =>
                                    importtance.id !== 0 ? (
                                        <option
                                            key={importtance.id}
                                            value={importtance.id}
                                            selected={
                                                this.state
                                                    .importanceOfTasksId &&
                                                parseInt(
                                                    this.state
                                                        .importanceOfTasksId,
                                                ) === importtance.id
                                                    ? true
                                                    : false
                                            }
                                        >
                                            {importtance.caption}
                                        </option>
                                    ) : null,
                            )}
                        </select>
                    </div>
                    <div className="form-container-input">
                        <label for="importanceOfTask" className="form-label">
                            Выберите дату и время окончания:
                        </label>
                        <input
                            key="finishDateTimeForTask"
                            id="finishDateTimeForTask"
                            className="form-input-date-time"
                            type="datetime-local"
                            onChange={this.handlerChangeFinishDateTimeForTask}
                            value={this.state.finishDateTimeForTask}
                        />
                    </div>
                    <div className="form-container-input">
                        <input
                            id="done"
                            type="checkbox"
                            onChange={this.handlerChangeDone}
                            checked={this.state.done || false}
                        />
                        <label for="done">Задача завершена?</label>
                    </div>
                    <div className="form-container-input">
                        <label for="importanceOfTask" className="form-label">
                            Дата и время завершения:
                        </label>
                        <input
                            key="dateDoneTask"
                            id="dateDoneTask"
                            className="form-input-date-time"
                            readonly={true}
                            type="datetime-local"
                            value={this.state.dateTimeDoneTask}
                        />
                    </div>

                    <div className="form-container-buttons">
                        <button className="form-buttons" type="submit">
                            {captionAddTaskButton}
                        </button>
                        <button
                            className="form-buttons"
                            onClick={this.handlerClose}
                        >
                            Отмена
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        taskList: getTaskList(state),
        title: getTitle(state),
        captionAddTaskButton: getCaptionAddTaskButton(state),
    };
}

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            fetchCurrentTaskList: bindActionCreators(
                fetchCurrentTaskList,
                dispatch,
            ),
            fetchAddTask: bindActionCreators(fetchAddTask, dispatch),
            fetchEditTask: bindActionCreators(fetchEditTask, dispatch),
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(TaskEditingForm);
