import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import moment from 'moment';
import { importanceOfTasks } from '../../dictionary';

import {
    fetchCurrentTaskList,
    fetchTitle,
    fetchCaptionButton,
} from '../../actions/TaskList';
import { getTaskList } from '../../reducers/TaskList';
import './TaskList.css';

class TaskList extends Component {
    constructor(props) {
        super(props);
    }

    // метод для щелчка по чекбоксу
    handlerOnCheckbox = event => {
        const { checked, id } = event.target;
        const { taskList, actions } = this.props;

        const dataCheckedIndex = taskList[parseInt(id)];
        let dateTimeDoneTask;
        // изменяем значение элемента
        dataCheckedIndex.done = checked;
        if (checked) {
            dataCheckedIndex.dateTimeDoneTask = moment().format(
                'YYYY-MM-DDTHH:mm',
            );
        } else {
            dataCheckedIndex.dateTimeDoneTask = null;
        }
        // создаем новый массв на основе старого
        const newtaskList = [...taskList];
        actions.fetchCurrentTaskList(newtaskList);
    };

    //метод для удаления задач
    handlerDeleteTask = event => {
        const { id } = event.target;
        const { taskList, actions } = this.props;
        const newtaskList = [...taskList];
        newtaskList.splice(id, 1);
        actions.fetchCurrentTaskList(newtaskList);
    };

    // метод для редактирования задач
    handlerEditTask = event => {
        const { history, actions } = this.props;
        actions.fetchTitle('Редактирование задачи');
        actions.fetchCaptionButton('Сохранение задачи');
        history.push(`/task-editing/${event.target.id}`);
    };

    // метод для добавления задач
    handlerAddTask = () => {
        const { history, actions } = this.props;
        actions.fetchTitle('Новая задача');
        actions.fetchCaptionButton('Добавить задачу');
        history.push('/task-editing');
    };

    //метод для фильтрации, закончить
    handlerChangeImportanceOfTask = event => {
        const { value } = event.target;
    };

    render() {
        const { taskList } = this.props;
        return (
            <div className="list-root">
                <div className="list-container">
                    <div className="list-container-filter">
                        <label for="importanceOfTask" className="form-label">
                            Фильтр по важности задачи:
                        </label>
                        <select
                            key="importanceOfTask"
                            id="importanceOfTask"
                            className="form-select"
                            placeholder="Выберите важность задачи"
                            onChange={this.handlerChangeImportanceOfTask}
                        >
                            {importanceOfTasks.map(importtance => (
                                <option
                                    key={importtance.id}
                                    value={importtance.id}
                                >
                                    {importtance.caption}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="list-container-buttons">
                        <button
                            className="list-buttons"
                            onClick={this.handlerAddTask}
                        >
                            Добавить задачу
                        </button>
                    </div>
                    {taskList &&
                        taskList.map((task, idx) => (
                            <div key={idx} className="list-item">
                                <input
                                    type="checkbox"
                                    id={idx}
                                    checked={task.done}
                                    onClick={this.handlerOnCheckbox}
                                />
                                <label
                                    for={idx}
                                    className={
                                        task.finishDateTimeForTask <
                                        moment().format('YYYY-MM-DDTHH:mm')
                                            ? 'list-item-overdue'
                                            : task.done
                                                ? 'list-item-done'
                                                : 'list-item-not-done'
                                    }
                                >
                                    {task.taskName}
                                </label>
                                <button id={idx} onClick={this.handlerEditTask}>
                                    Редактировать
                                </button>
                                <button
                                    id={idx}
                                    onClick={this.handlerDeleteTask}
                                >
                                    X
                                </button>
                            </div>
                        ))}
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        taskList: getTaskList(state),
    };
}

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            fetchCurrentTaskList: bindActionCreators(
                fetchCurrentTaskList,
                dispatch,
            ),
            fetchTitle: bindActionCreators(fetchTitle, dispatch),
            fetchCaptionButton: bindActionCreators(
                fetchCaptionButton,
                dispatch,
            ),
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
