export const importanceOfTasks = [
  {
    id: 0,
    caption: 'все',
  },
  {
    id: 1,
    caption: 'обычная',
  },
  {
    id: 2,
    caption: 'важная',
  },
  {
    id: 3,
    caption: 'очень важная',
  },
];
