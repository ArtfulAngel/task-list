import { Map } from 'immutable';
import { TaskListActions } from '../constants';

const initialState = Map({
    error: false,
    title: 'Новая задача',
    captionAddTaskButton: 'Добавить задачу',
    taskList: [],
});

function TaskListReducer(state = initialState, action) {
    switch (action.type) {
        case TaskListActions.TASK_LIST_SET:
            return state.set('taskList', action.payload);
        case TaskListActions.TASK_LIST_ADD:
            const taskList = state.get('taskList');
            return state.set('taskList', [...taskList, action.payload]);
        case TaskListActions.TASK_LIST_EDIT:
            let newTaskList = state.get('taskList');
            newTaskList.splice(
                parseInt(action.payload.id),
                1,
                action.payload.task,
            );

            return state.set('taskList', newTaskList);

        case TaskListActions.TASK_LIST_SET_TITLE:
            return state.set('title', action.payload);
        case TaskListActions.TASK_LIST_SET_CAPTION_BUTTON:
            return state.set('captionAddTaskButton', action.payload);
        case TaskListActions.TASK_LIST_FAILURE:
            return state.set('error', false);
        default:
            return state;
    }
}

export function getTaskList(state) {
    if (state.TaskListReducer.get('taskList') == null) {
        return null;
    } else {
        return state.TaskListReducer.get('taskList');
    }
}

export function getTitle(state) {
    if (state.TaskListReducer.get('title') == null) {
        return null;
    } else {
        return state.TaskListReducer.get('title');
    }
}
export function getCaptionAddTaskButton(state) {
    if (state.TaskListReducer.get('captionAddTaskButton') == null) {
        return null;
    } else {
        return state.TaskListReducer.get('captionAddTaskButton');
    }
}

export default TaskListReducer;
