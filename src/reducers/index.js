// Redux
import { combineReducers } from 'redux';

import TaskListReducer from './TaskList';

export default combineReducers({
    TaskListReducer,
});
