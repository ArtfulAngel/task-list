import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import TaskList from '../components/TaskList';
import TaskEditingForm from '../components/TaskEditingForm';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={TaskList} />
      <Route exact path="/task-editing/:id" component={TaskEditingForm} />
      <Route path="/task-editing" component={TaskEditingForm} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
